﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPractModule14
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, List<int>> statistic = new Dictionary<string, List<int>>();
            int uniqueCount = 0;
            string data = "Вот дом, Который построил Джек. А это пшеница, Кото­рая в темном  чулане хранится В доме, Который построил Джек. А это веселая птица­ синица, Которая часто вору­ет пшеницу, Которая в темном чулане хранится В доме, Который построил Джек.";
            string[] words = data.ToLower().Split(' ');

            for (int i = 0; i < words.Length; i++)
            {
                if (!statistic.ContainsKey(words[i]))
                {
                    statistic.Add(words[i], new List<int> { i });
                    uniqueCount++;
                }
                else
                {
                    statistic[words[i]].Add(i);
                }
            }
            int number = 1;
            Console.WriteLine("\tWord:\t\tcount:");
            foreach (var item in statistic)
            {
                Console.Write($"{number}.\t{item.Key}\t\t{item.Value.Count()}");
                Console.WriteLine();
                if (item.Value.Count == 1) { uniqueCount++; }
                number++;
            }
            Console.WriteLine($"All word count: {words.Count()}. Unique words: {uniqueCount}");
            Console.ReadLine();
        }
    }
}
